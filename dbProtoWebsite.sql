-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.2-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for dbprotowebsite
DROP DATABASE IF EXISTS `dbprotowebsite`;
CREATE DATABASE IF NOT EXISTS `dbprotowebsite` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbprotowebsite`;

-- Dumping structure for table dbprotowebsite.tlogs
DROP TABLE IF EXISTS `tlogs`;
CREATE TABLE IF NOT EXISTS `tlogs` (
  `LogId` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `Type` enum('Regular','Important','Urgent') NOT NULL DEFAULT 'Urgent',
  `UserId` int(10) unsigned zerofill DEFAULT NULL,
  `EventTime` datetime NOT NULL,
  `AddDetails` text NOT NULL,
  PRIMARY KEY (`LogId`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `UserId` FOREIGN KEY (`UserId`) REFERENCES `tuser` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Logs table';

-- Dumping data for table dbprotowebsite.tlogs: ~0 rows (approximately)
/*!40000 ALTER TABLE `tlogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tlogs` ENABLE KEYS */;

-- Dumping structure for table dbprotowebsite.tuser
DROP TABLE IF EXISTS `tuser`;
CREATE TABLE IF NOT EXISTS `tuser` (
  `Id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `AccountType` enum('ADMIN','MOD','USER') NOT NULL DEFAULT 'USER',
  `Email` varchar(60) NOT NULL,
  `Username` varchar(60) NOT NULL,
  `Password` varchar(60) NOT NULL,
  `IsValidated` enum('NOT_VALIDATED','VALID') NOT NULL DEFAULT 'NOT_VALIDATED',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Email` (`Email`),
  UNIQUE KEY `Username` (`Username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='users table';

-- Dumping data for table dbprotowebsite.tuser: ~2 rows (approximately)
/*!40000 ALTER TABLE `tuser` DISABLE KEYS */;
INSERT IGNORE INTO `tuser` (`Id`, `AccountType`, `Email`, `Username`, `Password`, `IsValidated`) VALUES
	(00000000000, 'USER', 'jcl@ntek.ca', 'admin', '$2b$10$aqRxL45mAG6HeWkvLQ2xROxjmegE3LdC6wKoxySL4vHp382GXD5Ge', 'NOT_VALIDATED'),
	(00000000001, 'USER', 'test@test.com', 'test', '$2b$10$yvyuU41mWUi8ztRFzZNWqOybSBstaAfdlXuEoV8bYumg7YJyAfRku', 'NOT_VALIDATED');
/*!40000 ALTER TABLE `tuser` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
