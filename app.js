var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('mysql');
var moment = require('moment');

// Set database information
var con = mysql.createConnection({
  host: "localhost",
  user: "nodeJs",
  password: "RAFADOQ8",
  database: "dbProtoWebsite"
});

// Initialise routers
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// Set up users sessions' options 
app.use(session({
  secret: 'Shut up, this is my secret', // Secret to change, this is temporary
  cookie: { maxAge: (5 * (60 * 1000)) },// (5*(60*1000)) = 5 min
  resave: true,
  saveUninitialized: true
})
);

// view engine setup
app.set('views', path.join(__dirname, 'views/pages'));
app.set('view engine', 'ejs');

// Uncomment after placing and renaming the favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.session = req.session;
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  res.session = req.session;
  // set locals, only providing error in development
  //res.locals.message = (err.message || "Oh no, somehting went wrong on our side... Please try again or contact our support");
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // set the err status to 500 if not already defined
  res.status(err.status || 500);

  if (err.status !== 404) {
    if (typeof req.session.userid !== 'undefined') {
      var now = moment().format().slice(0, 19).replace('T', ' ');
      var query = "INSERT INTO tLogs (Type,UserId,EventTime,AddDetails) VALUES ('SERVER_ERROR','" + req.session.userid + "','" + now + "','" + err.status + " - " + res.locals.message + "');";
      con.query(query, function (error) {
        if (error) {
          var query = "INSERT INTO tLogs (Type,UserId,EventTime,AddDetails) VALUES ('DB_ERROR','" + req.session.userid + "','" + now + "',\"" + error + "\");";
            con.query(query, function (err) {
              if (err) {
                console.log(err); // We will stop logging here, or we will redo inception...
              }
            });
        }
      });
    } else {
      var now = moment().format().slice(0, 19).replace('T', ' ');
      var query = "INSERT INTO tLogs (Type,EventTime,AddDetails) VALUES ('SERVER_ERROR','" + now + "','" + err.status + " - " + res.locals.message + "');";
      con.query(query, function (error) {
        if (error) {
          var query = "INSERT INTO tLogs (Type,UserId,EventTime,AddDetails) VALUES ('DB_ERROR','" + req.session.userid + "','" + now + "',\"" + error + "\");";
            con.query(query, function (err) {
              if (err) {
                console.log(err); // We will stop logging here, or we will redo inception...
              }
            });
        }
      });
    }
  }


  // render the error page
  res.render('error', { status: err.status });
});

module.exports = app;
