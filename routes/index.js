var createError = require('http-errors');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.session = req.session;
  res.render('index', { title: 'Express' ,Username: req.session.username});
});

router.get('/camera/1', function(req, res, next) {
  next(createError(500));
});

module.exports = router;
