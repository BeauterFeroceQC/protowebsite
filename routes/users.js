var createError = require('http-errors');
var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var moment = require('moment');

var con = mysql.createConnection({
  host: "localhost",
  user: "nodeJs",
  password: "RAFADOQ8",
  database: "dbProtoWebsite"
});

var bcrypt = require('bcrypt');

/* GET users listing. */
router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

router.post('/login', function(req, res, next) {
  var query = "SELECT * FROM tUser WHERE Username=\"" + req.body.username + "\";";
  con.query(query, function(err, result) {
    if(err) {
      console.log(err);
    } else {
      if(typeof result[0] !== 'undefined' && result[0]) {
        keys = result.map((o) => {
          return Object.keys(o);
        }).reduce((prev, curr) => {
          return prev.concat(curr);
        });
        var response = bcrypt.compareSync(req.body.password,result[0].Password);
            if(response) {
              var session = req.session;
              session.username = result[0].Username;
              session.userid = result[0].Id;
              res.redirect('/');
            } else {
              var session = req.session;
              session.username = null;
              session.userid = 0;
              res.render('error', { status: null, message:'Incorrect username or password'});
            }
      } else {
        res.render('error', { status: null, message:'Incorrect username or password'});
      }
    }
  });
});

router.get('/logout', function(req, res, next) {
  req.session.username = null;
  res.redirect('/');
});

router.get('/signup', function(req, res, next) {
  res.render('signup', { title: 'signup' });
});

router.post('/signup', function(req, res, next) {
  var query = "SELECT * FROM tUser WHERE Username=\"" + req.body.username + "\" OR Email=\"" + req.body.email + "\";";
    con.query(query, function(err, result) {
      if(err) {
        console.log(err);
        next(createError(500));
      } else {
        if(typeof result[0] !== 'undefined' && result[0]) {
          res.render('error',{status: '', message: 'Username or e-mail address already in use'});
        } else {
          var now = moment().format().slice(0, 19).replace('T', ' ');
          query = "INSERT INTO tUser (AccountType,Email,Username,Password) VALUES ('USER','"+req.body.email+"','"+req.body.username+"','"+bcrypt.hashSync(req.body.password,10)+"');";
          con.query(query, function(err) {
            if(err) {
              var query = "INSERT INTO tLogs (Type,EventTime,AddDetails) VALUES ('DB_ERROR','" + now + "',\"" + err + "\");";
                con.query(query, function (err) {
                  if (err) {
                    console.log(err); // We will stop logging here, or we will redo inception...
                  }
              });
              var error = err;
              error.message = "Internal Server Error";
              error.status = 500
              res.render('error', {status: error.status, message:error.message});
            } else {
              var session = req.session;
              session.username = req.body.username;
              res.redirect('/');
            }
          });
        }
      }
    });
});

module.exports = router;
